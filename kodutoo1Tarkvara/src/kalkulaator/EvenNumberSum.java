package kalkulaator;

/**
 * Created by Ilya on 06/10/2015.
 */

public class EvenNumberSum {
    public static int evenNumberSum(int[] ints) {
        int sum = 0;
        for(int i: ints) {
            if (i%2 == 0 && i > 0) {
                sum += i;
            }
        }
        return sum;
    }
}
