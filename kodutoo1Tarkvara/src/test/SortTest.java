package test;

/**
 * Created by Ilya on 06/10/2015.
 */
import static org.junit.Assert.*;
import kalkulaator.Sort;

import org.junit.Test;

public class SortTest {

    @Test
    public void testSortEmptyArray() {
        assertArrayEquals(new int[] {}, Sort.sort(new int[] {}));
    }

    @Test
    public void testSortOneElementArray() {
        assertArrayEquals(new int[] { 1 }, Sort.sort(new int[] { 1 }));
    }

    @Test
    public void testSortTwoElementsArrayInRightOrder() {
        assertArrayEquals(new int[] { 1, 2 }, Sort.sort(new int[] { 1, 2 }));
    }

    @Test
    public void testSortTwoElementsArrayInWrongOrder() {
        assertArrayEquals(new int[] { 1, 2 }, Sort.sort(new int[] { 2, 1 }));
    }

    @Test
    public void testSortThreeElementsArrayInRightOrder() {
        assertArrayEquals(new int[] { 1, 6, 10 },
                Sort.sort(new int[] { 1, 6, 10 }));
    }

    @Test
    public void testSortThreeElementsArrayInWrongOrder() {
        assertArrayEquals(new int[] { 1, 6, 92 },
                Sort.sort(new int[] { 6, 92, 1 }));
    }

    @Test
    public void testSortLongArrayInWrongOrder() {
        int[] unsorted = new int[100];
        int[] sorted = new int[100];
        for (int i = 0; i < 100; i++) {
            sorted[i] = i;
            unsorted[i] = 99 - i;
        }
        assertArrayEquals(sorted, Sort.sort(unsorted));
    }
}
